.. index:: Tags

.. Mark as "orphan" until extensions is out of beta.

:orphan:

###################
  Extensions Tags
###################

This is the list of the tags currently supported:

* 3D View
* Add Mesh
* Add Curve
* Animation
* Compositing
* Development
* Game Engine
* Import-Export
* Lighting
* Material
* Modeling
* Mesh
* Node
* Object
* Paint
* Pipeline
* Physics
* Render
* Rigging
* Scene
* Sequencer
* System
* Text Editor
* UV
* User Interface
